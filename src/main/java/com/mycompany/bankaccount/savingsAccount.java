package com.mycompany.bankaccount;

public class SavingsAccount extends BankAccount
{
    private double interestRate;
    private double minBalance;

    /**
     * Constructor
     */
    public SavingsAccount()
    {

    }

    /**
     * Constructs a savings account with an initial balance
     * @param initialBalance The balance to set when creating the account
     * @param min
     * @param rate
     */
    public SavingsAccount(double initialBalance, double min, double rate)
    {
        super(initialBalance);
        this.minBalance = min;
        this.interestRate = rate;
    }
    /**
     * Get the current interest rate
     * @return Current interest rate
     */
    public double getInterestRate()
    {
        return interestRate;
    }

    /**
     * Get the current minimum balance
     * @return Current minimum balance
     */
    public double getMinBalance()
    {
        return minBalance;
    }

    /**
     * Sets the interest rate
     * @param rate
     */
    public void setInterestRate(double rate)
    {
    if (rate > 0)
        {
        this.interestRate=rate;
        }
    else
        {
        rate = 0.0;
        }
    }
    
    public void setMinBalance(double balance)
        {
        if (balance>0)
            {
            
            }
        else
            {
            
            }
        }

    //Override the following methods
    /**
     * Withdraws money from the account.
     * @param amount
     * @return boolean
     */
    @Override
    public boolean withdraw(double amount)
    {
    boolean success;
    success = super.withdraw(amount);
    double balance = getBalance();
    if(balance < minBalance)
        {
        minBalance = balance;
        }
    return success;
    }

    //Calculate compound interest
    @Override
    /**
     * monthEnd calculates and adds the interest.
     * Interest calculated on min balance in account during month.
     */
    public void monthEnd()
    {
    double interest = minBalance * (interestRate/100);
    deposit(interest);
    minBalance = getBalance();
    }
}
