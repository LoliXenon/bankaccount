package com.mycompany.bankaccount;

public class CheckingAccount extends BankAccount
{
    private int withdrawals;

    /**
     * Constructs a checking account
     */
    public CheckingAccount()
    {

    }

    /**
     * Construct a checking account with an initial balance
     * @param initialBalance The initial balance for the account
     */
    public CheckingAccount(double initialBalance)
    {
        super(initialBalance);
    }

    /**
     * Returns the number of withdrawals
     * @return Number of withdrawals
     */
    public int getWithdrawals()
    {
        return withdrawals;
    }

    //Override the following methods
    public boolean withdraw(double amount)
    {
        return true;
    }

    public void monthEnd()
    {

    }
}
