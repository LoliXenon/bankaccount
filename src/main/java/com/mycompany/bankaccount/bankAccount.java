package com.mycompany.bankaccount;

/**
 * @author TheLazyHatGuy
 */
public class BankAccount
{
    private double balance;

    /**
     * Constructor
     */
    public BankAccount()
    {
        this.balance = 0;
    }

    /**
     * Construct a bank account with an initial balance
     * @param initialBalance The initial balance for the account
     */
    public BankAccount(double initialBalance)
    {
        if (initialBalance > 0.0)
        {
            this.balance = initialBalance;
        }
        else
        {
            this.balance = 0;
        }
    }

    /**
     * Gets the current balance of the account
     * @return current balance
     */
    public double getBalance()
    {
        return this.balance;
    }

    /**
     * Makes a deposit into the account
     * @param amount to deposit
     */
    public void deposit(double amount)
    {
        int decimalPlaces = numberOfDecimalPlaces(amount);

        if (amount > 0 && decimalPlaces <= 2)
        {
            this.balance += amount;
        }
    }

    /**
     * Makes a withdraw from the account
     * @param amount Amount to withdraw
     * @return If the withdraw was successful or not
     */
    public boolean withdraw(double amount)
    {
        if (amount > 0  && amount <= getBalance())
        {
            balance -= amount;
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Carries out end of month process
     * Depends on the account
     */
    public void monthEnd()
    {

    }

    /**
     * Finds the number of decimal places in a double
     * @param amount The double you want to find the number of decimal places for
     * @return Number of decimal places
     */
    public int numberOfDecimalPlaces(double amount)
    {
        String text = Double.toString(Math.abs(amount));
        int decimalIndex = text.indexOf('.');
        int decimalPlaces = text.length() - decimalIndex - 1;

        return decimalPlaces;
    }
}
