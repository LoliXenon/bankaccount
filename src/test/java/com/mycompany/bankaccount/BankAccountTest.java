package com.mycompany.bankaccount;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BankAccountTest
{
    @Test
    void deposit()
    {
        System.out.println("---Deposit---");

        BankAccount instance = new BankAccount(0);

        System.out.println("Standard Deposit");
        instance.deposit(11.50);
        assertEquals(11.5, instance.getBalance());

        System.out.println("0 Deposit");
        instance.deposit(0.0);
        assertEquals(11.50, instance.getBalance());

        System.out.println("Negative Deposit");
        instance.deposit(-100.0);
        assertEquals(11.50, instance.getBalance());

        System.out.println("Long Decimal deposit");
        instance.deposit(3.333333);
        assertEquals(11.50, instance.getBalance());
    }

    @Test
    void withdraw()
    {
        System.out.println("---Withdraw---");

        BankAccount instance = new BankAccount(100);
        boolean expectedResult;
        boolean result;

        System.out.println("Withdraw 0");
        expectedResult = false;
        result = instance.withdraw(0);
        assertEquals(expectedResult, result);

        System.out.println("Withdraw money from account");
        testWithdraw(instance, 50, 50, true);
        System.out.println("Withdraw more than is in the account");
        testWithdraw(instance, 100, 50, false);
        System.out.println("Withdraw negative amount");
        testWithdraw(instance, -100, 50, false);
        System.out.println("Withdraw char");
        testWithdraw(instance, 'f', 50, false);
        testWithdraw(instance, 'A', 50, false);
        testWithdraw(instance, '/', 50, false);
        System.out.println("Withdraw 0");
        testWithdraw(instance, 0.00, 50, false);
    }

    public void testWithdraw(BankAccount accountToWithdrawFrom, double amount,
                             double expectedBalance, boolean expectedResult)
    {
        boolean result;
        result = accountToWithdrawFrom.withdraw(amount);

        assertEquals(expectedResult, result);
        assertEquals(expectedBalance, accountToWithdrawFrom.getBalance());
    }

    @Ignore
    void monthEnd()
    {
        System.out.println("Bank Account MonthEnd");
    }

    @Test
    void getBalance()
    {
        System.out.println("Get Balance");
        BankAccount instance = new BankAccount(100);

        assertEquals(100, instance.getBalance());
    }
}