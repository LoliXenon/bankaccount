package com.mycompany.bankaccount;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SavingsAccountTest {

    @Test
    void setInterestRate()
    {
        System.out.println("Set Interest Rate");
        SavingsAccount instance = new SavingsAccount();

        instance.setInterestRate(10.0);
        assertEquals(10.0, instance.getInterestRate());
    }

    @Test
    void withdraw()
    {
        System.out.println("Savings Account Withdraw");
        SavingsAccount instance = new SavingsAccount(500);

        instance.withdraw(100);
        assertEquals(400, instance.getBalance());
        assertEquals(400, instance.getMinBalance());

        instance.deposit(200);
        assertEquals(600, instance.getBalance());
        assertEquals(400, instance.getMinBalance());
    }

    @Test
    void monthEnd()
    {
        System.out.println("Savings Account Month End");
        SavingsAccount instance = new SavingsAccount(1000);

        instance.setInterestRate(1.0);
        instance.withdraw(500);
        instance.deposit(250);
        instance.withdraw(450);
        instance.monthEnd();

        assertEquals(303, instance.getBalance());
    }
}