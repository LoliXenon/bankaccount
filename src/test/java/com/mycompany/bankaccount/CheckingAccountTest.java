package com.mycompany.bankaccount;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckingAccountTest
{

    @Test
    void withdraw()
    {
        CheckingAccount instance = new CheckingAccount(1000);

        instance.withdraw(100);
        instance.withdraw(100);
        instance.withdraw(100);
        assertEquals(700, instance.getBalance());
        assertEquals(3, instance.getWithdrawals());

        instance.withdraw(100);
        assertEquals(599, instance.getBalance());
        assertEquals(4, instance.getWithdrawals());
    }

    @Test
    void monthEnd()
    {
        CheckingAccount instance = new CheckingAccount(1000);

        instance.withdraw(100);
        instance.withdraw(100);
        instance.withdraw(100);
        assertEquals(700, instance.getBalance());
        assertEquals(3, instance.getWithdrawals());

        instance.monthEnd();
        assertEquals(700, instance.getBalance());
        assertEquals(0, instance.getWithdrawals());
    }
}